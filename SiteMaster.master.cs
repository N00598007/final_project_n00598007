﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace Final_Project
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void search_page(object sender, EventArgs args)
        {
            string key = search_title.Text;
            Response.Redirect("Search.aspx?key=" + key);

            Response.Redirect("Search.aspx");
        }
        protected void Page_Load(object sender, EventArgs e){
            string query = "select * from pages";
            page_select.SelectCommand = query;
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            DataRowView pagerowview;
            int view_length = pageview.ToTable().Rows.Count;
            menu_list.InnerHtml = "<li><a href ="+"Home.aspx"+">Home</a></li>";
            for (int i = 0; i < view_length; i++)
            {
                pagerowview = pageview[i];
                menu_list.InnerHtml += "<li><a href=" + "/Pages.aspx?pageid=" + pagerowview["pageid"] + ">" + pagerowview["pagetitle"].ToString() + "</a></li> ";
            }
        }     
    }
}

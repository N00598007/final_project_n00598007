﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="Pages.aspx.cs" Inherits="Final_Project.Pages" Theme="masterpages"%>

<asp:Content runat="server" ContentPlaceHolderID="page">
    <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:sql_con%>" id="page_select" runat="server">
                
    </asp:SqlDataSource>  
    <div class="container">
        <h1 id="page_title" runat="server"></h1>
        <p id="page_content" runat="server"></p>
        <p id="publish_date" runat="server"></p>
    </div>
    <div id="debug" runat="server">
     
    </div>
</asp:Content>

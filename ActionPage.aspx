﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="ActionPage.aspx.cs" Inherits="Final_Project.ActionPage" Theme="masterpages"%>

<asp:Content runat="server" ContentPlaceHolderID="page">
    <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:sql_con%>" id="page_select" runat="server">
                
    </asp:SqlDataSource>
    <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:sql_con%>" id="edit_page" runat="server">
                
    </asp:SqlDataSource>
    <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:sql_con%>" id="delete_page" runat="server">
                
    </asp:SqlDataSource>
    <div class="center">
        <h1>Edit Page</h1>
        <label>Page Title</label><br>
        <asp:TextBox id="page_title" runat="server" Placeholder="Page Title"/><br>
        <label>Page Content</label><br>
        <asp:TextBox id="page_content" TextMode="multiline" Columns="55" Rows="10" Font-Size="16px" runat="server"  Placeholder="Page Content"/><br>
        <asp:Button class="edit" runat="server" Text="Edit" OnClick="Edit_Page"/>
        <asp:Button class="delete" runat="server" Text="Delete" OnClick="Del_Page" OnClientClick="if(!confirm('Are you sure?')) return false;"/><br>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project
{
    public partial class ActionPage : System.Web.UI.Page
    {
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (pageid == "" || pageid == null)
            {
                page_title.Text = "";
                page_content.Text = "";
            }
         
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (pageid == "" || pageid == null)
            {
                page_title.Text = "";
                page_content.Text = "";
            }else{
                DataRowView pagerow = getPageInfo(Int32.Parse(pageid));
                page_title.Text = pagerow["pagetitle"].ToString();
                page_content.Text = pagerow["pagecontent"].ToString();
            }
        }
        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid;
            page_select.SelectCommand = query;
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0]; 
            return pagerowview;
        }
        protected void Del_Page(object sender, EventArgs e)
        {
            string query = "DELETE FROM PAGES WHERE PAGEID = "+pageid;

            delete_page.DeleteCommand = query;
            delete_page.Delete();
            Response.Redirect("Home.aspx");

            //debug.InnerHtml = query;                  
        }    
        protected void Edit_Page(object sender, EventArgs e)
        {
            string pagetitle = page_title.Text;
            string pagecontent = page_content.Text;

            string query = "UPDATE PAGES SET PAGETITLE='" + pagetitle + "'," + "PAGECONTENT='" + pagecontent + "'" +
                "WHERE PAGEID =" + pageid;
            edit_page.UpdateCommand = query;
            edit_page.Update();

            Page.Response.Redirect(Page.Request.Url.ToString(), true);
           
        }
    }
}

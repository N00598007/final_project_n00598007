﻿using System;
using System.Web;
using System.Web.UI;

namespace Final_Project
{

    public partial class AddPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e){
        }
        protected void Add_Page(object sender, EventArgs args)
        {
            string pagetitle = page_title.Text;
            string pagecontent = page_content.Text;
            string today = DateTime.Now.ToString("yyyy/mm/dd");

            page_title.Text = "";
            page_content.Text = "";

            string addPageQuery = "insert into pages (pagetitle, pagecontent, publish_date) values ('" + pagetitle + "'," + "'" + pagecontent + "', switchoffset (CONVERT(datetimeoffset, GETDATE()), '-05:00'))";
            insert_page.InsertCommand = addPageQuery;
            insert_page.Insert();

            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            result_msg.InnerHtml = addPageQuery;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project
{
    public partial class Pages : System.Web.UI.Page
    {
        public string pageid{
            get { return Request.QueryString["pageid"]; }
        }
        protected void Page_Load(object sender, EventArgs e){
            if (pageid == "" || pageid == null)
            {
                debug.InnerHtml = "Page id is not found";
            }         
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            string query = "select * from pages where pageid ="+pageid;
            page_select.SelectCommand = query;
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            DataRowView pagerowview = pageview[0];

            page_title.InnerHtml = pagerowview["pagetitle"].ToString()+"<br>";
            page_content.InnerHtml = pagerowview["pagecontent"].ToString()+"<br><br>";
            publish_date.InnerHtml = "Publish Date:<br>"+pagerowview["publish_date"].ToString();
        }
    }
}

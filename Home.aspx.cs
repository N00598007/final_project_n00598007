﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project
{
    public partial class Home : Page
    {
        private string page_list_query = "SELECT * FROM PAGES";

        protected void Page_Load(object sender, EventArgs e)
        {
            page_select.SelectCommand = page_list_query;
            page_list.DataSource = Page_Manual_Bind(page_select);

            page_list.DataBind();
        }
        protected DataView Page_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            DataColumn editcol = new DataColumn();
            editcol.ColumnName = "ACTION";
            editcol.DefaultValue = "EDIT/DELETE";

            mytbl.Columns.Add(editcol);
            foreach (DataRow row in mytbl.Rows)
            {
                row[editcol] =
                    "<a href=\"ActionPage.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row[editcol]
                    + "</a>";           
            }
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;
        }
        protected void Add_Page(object sender, EventArgs args)
        {
            Response.Redirect("AddPage.aspx", false);
        }     
    }
}

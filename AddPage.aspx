﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="AddPage.aspx.cs" Inherits="Final_Project.AddPage" Theme="masterpages"%>

<asp:Content runat="server" ContentPlaceHolderID="add_page">
       <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:sql_con%>" id="insert_page" runat="server">
                
       </asp:SqlDataSource>
    
       <div class="center">
            <h1>Publish New Content</h1>
            <div>
                <asp:TextBox id="page_title" runat="server" Placeholder="Page Title"/>
                <asp:RequiredFieldValidator ID="title_validator" runat ="server" ControlToValidate ="page_title" ErrorMessage="Please enter page title" ForeColor="Red"></asp:RequiredFieldValidator>
            </div>
            <div>
                <asp:TextBox id="page_content" TextMode="multiline" Columns="50" Rows="10" runat="server"  Placeholder="Page Content"/>
                <asp:RequiredFieldValidator ID="content_validator" runat ="server" ControlToValidate ="page_content" ErrorMessage="Please enter page content" ForeColor="Red"></asp:RequiredFieldValidator>
            </div>
            <asp:Button class="publish" runat="server" Text="Publish" OnClick="Add_Page"/><br><br>
       </div>
    
       <div id="result_msg" runat="server">
     
        </div>
    
</asp:Content>

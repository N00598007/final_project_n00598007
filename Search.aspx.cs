﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project
{
    public partial class Search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e){
            string key = Request.QueryString["key"];
            string query = "SELECT * FROM PAGES WHERE PAGETITLE LIKE '%";
         

            title.InnerHtml = "Result Search of '" + key + "'";

            page_select.SelectCommand = query + key + "%' OR PAGECONTENT LIKE '%"+ key+"%'" ;
            page_list.DataSource = Page_Manual_Bind(page_select);

            page_list.DataBind();
        }
        protected DataView Page_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            DataColumn editcol = new DataColumn();
            editcol.ColumnName = "ACTION";
            editcol.DefaultValue = "EDIT/DELETE";

            mytbl.Columns.Add(editcol);

            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row[editcol] =
                    "<a href=\"ActionPage.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row[editcol]
                    + "</a>";
            }

            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;
        }
    }
}
